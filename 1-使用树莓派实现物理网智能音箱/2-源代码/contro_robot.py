#!/usr/bin/python3.7
#encoding:utf-8
import time
import serial.tools.list_ports
#
"""串口发送和接收在两个类中打开使用，萹蓄在两个线程中运行
    实际测试过程中发现，串口读取和发送使用同一个串口对象会发生接受串口信息丢失的现象
    需要在两个子线程中分别实例化两个串口对象，一个用来发送，一个接收"""
"""向串口发送数据类"""
class SerialControRobot():
    def __init__(self):
        self.plist = list(serial.tools.list_ports.comports())
        if len(self.plist) <= 0:
            print("没有发现端口!")
        else:
            self.plist_0 = list(self.plist[0])
            serialName = self.plist_0[0]
            self.serialFd = serial.Serial(serialName, 9600, timeout=60)
            print("正在建立串口发送！ 可用端口名>>>"+self.serialFd.name)
        self.ser = serial.Serial(self.serialFd.name, 115200)  # 设置了第一个串口号，波特率，读超时设置
        print("正常连接中")
#
    '''PC端向单片机通过串口发送数据'''
    def sand_message(self,message):
        message = str(message)
        self.ser.write((message+'\r\n').encode())
#-
    """测试方法，无实际意义"""
    def fun(self):
        while True:
            self.sand_message('10')
            time.sleep(0.2)
            self.sand_message('01')
            time.sleep(0.2)
#
"""从串口接受数据类"""
class SerialReceveRobot():
    def __init__(self):
        self.plist = list(serial.tools.list_ports.comports())
        if len(self.plist) <= 0:
            print("没有发现端口!")
        else:
            self.plist_0 = list(self.plist[0])
            serialName = self.plist_0[0]
            self.serialFd = serial.Serial(serialName, 9600, timeout=60)
            print("正在建立串口接收端！ 可用端口名>>>",self.serialFd.name)
            self.ser = serial.Serial(self.serialFd.name, 115200)  # 建立串口接受的对象，设置了第一个串口号，波特率，读超时设置
            print("正常连接中")
#
    '''PC端通过串口接收单片机数据'''
    def recive_serial(self):
        count = self.ser.inWaiting()
        if count > 0:
            recive_serial = self.ser.read(count)
            return recive_serial

    '''PC端通过串口接收单片机数据'''

    def recive_serial_2(self):
        self.count = self.ser.inWaiting()
        if self.count > 0:
            return self.count
            # print("PC端串口接受到的信息：", self.ser.read(self.count))


if __name__ == "__main__":
    a = SerialControRobot()
    a.fun()
    #while True:
        
"""
        while True:
            self.count = self.ser.inWaiting()
            if self.count > 0:
                print("PC端串口接受到的信息：", self.ser.read(self.count))
                recive = self.ser.read(self.count)
                #if recive == b'01\r\n':
                    #print("识别成功")
"""

