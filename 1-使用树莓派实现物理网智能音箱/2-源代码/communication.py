#!/usr/bin/python3.7
# coding=utf-8
#客户端与上一个没有任何改变
from socket import *
import threading

"""
此模块用来用来和物联网模块及逆行TCP通讯。
目前实现在局域网内进行物联网设备的通信控制。
物联网模块使用的是esp8266.通过局域网连接到模块的地址，发送和接收特定的模块
"""
class communicat():
    """"""
    """物联网模块初始化，默认的ip地址是原来的模块连接家的WiFi的IP地址
        端口号可以更改
    """
    def __init__(self,a = "192.168.43.10",p = 8266):
        self.address = a  #服务器的ip地址
        self.port = p           #服务器的端口号
        self.buffsize=1024        #接收数据的缓存大小
        self.s=socket(AF_INET, SOCK_STREAM)
        self.s.connect((self.address,self.port))

    """从物联网模块直接接收数据并返回"""
    def reve_value(self):
        while True:
            recvdata = self.s.recv(self.buffsize).decode('utf-8')
            print("\n物联网接收的数据是：" + recvdata)
            if recvdata :
                return recvdata
            else:
                self.break_out()
                self.__init__(self.address,self.port)

    """给物联网模块发送数据。"""
    def sand_value(self,value):
        #senddata=input('\n想要发送的数据：' )
        self.s.send(value.encode())

    """切断链接"""
    def break_out(self):
        self.s.close()

"""下面是测试代码"""

print("begin")
a = communicat()
def fun():
    while True:
        x = a.reve_value()
        print("receive is :"+x)
if __name__ == '__main__':
    t = threading.Thread(target=fun)  # t为新创建的线程
    t.start()
    while True:
        senddata=input('\n想要发送的数据：' )
        a.sand_value(senddata)


